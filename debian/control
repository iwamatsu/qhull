Source: qhull
Section: math
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Barak A. Pearlmutter <bap@debian.org>
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 11),
	       dh-exec (>= 0.3),
	       cmake,
	       chrpath,
	       docbook2x (>= 0.8.8-3), xsltproc, docbook-xml
Homepage: https://www.qhull.org
Vcs-Git:  https://salsa.debian.org/science-team/qhull.git
Vcs-Browser: https://salsa.debian.org/science-team/qhull

Package: libqhull7
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: calculate convex hulls and related structures (shared library)
 Qhull computes convex hulls, Delaunay triangulations, halfspace
 intersections about a point, Voronoi diagrams, furthest-site
 Delaunay triangulations, and furthest-site Voronoi diagrams. It
 runs in 2-d, 3-d, 4-d, and higher dimensions.
 .
 This package contains the shared C library.

Package: libqhull-r7
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: calculate convex hulls and related structures (reentrant shared library)
 Qhull computes convex hulls, Delaunay triangulations, halfspace
 intersections about a point, Voronoi diagrams, furthest-site
 Delaunay triangulations, and furthest-site Voronoi diagrams. It
 runs in 2-d, 3-d, 4-d, and higher dimensions.
 .
 This package contains the reentrant version of the shared C library.

Package: libqhull-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libqhull7 (= ${binary:Version}), libqhull-r7 (= ${binary:Version}), ${misc:Depends}
Description: calculate convex hulls and related structures (development files)
 Qhull computes convex hulls, Delaunay triangulations, halfspace
 intersections about a point, Voronoi diagrams, furthest-site
 Delaunay triangulations, and furthest-site Voronoi diagrams. It
 runs in 2-d, 3-d, 4-d, and higher dimensions.
 .
 This package contains the files necessary for development (headers and
 libraries), as well as the library documentation in HTML format.

Package: libqhull-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: calculate convex hulls and related structures (documentation files)
 Qhull computes convex hulls, Delaunay triangulations, halfspace
 intersections about a point, Voronoi diagrams, furthest-site
 Delaunay triangulations, and furthest-site Voronoi diagrams. It
 runs in 2-d, 3-d, 4-d, and higher dimensions.
 .
 This package contains the documentation for Qhull.

Package: qhull-bin
Architecture: any
Depends: libqhull7 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Suggests: geomview
Description: calculate convex hulls and related structures (utilities)
 Qhull computes convex hulls, Delaunay triangulations, halfspace
 intersections about a point, Voronoi diagrams, furthest-site
 Delaunay triangulations, and furthest-site Voronoi diagrams. It
 runs in 2-d, 3-d, 4-d, and higher dimensions.
 .
 This package contains the qhull executable that gives a pipe interface to
 some of the functionality of the library.  Also included is rbox is a
 useful tool in generating input for Qhull; it generates hypercubes,
 diamonds, cones, circles, simplices, spirals, lattices, and random points.
 .
 Qhull produces graphical output for Geomview.  This helps with
 understanding the output (http://www.geomview.org).
